package Encryption;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Provider.Service;
import java.security.SecureRandom;
import java.security.Security;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class EncryptionManager {
	
	private Key pubKey;
	private Key privKey;
	private Cipher cipher;
	
	public EncryptionManager()
	{
		try {
			cipher = Cipher.getInstance("RSA", "SunJCE");
		} catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void genKeyPair()
	{
		SecureRandom random = new SecureRandom();
	    KeyPairGenerator generator=null;
		try {
			generator = KeyPairGenerator.getInstance("RSA","SunRsaSign");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		System.err.println(generator.getProvider().toString());
//		Provider[] bb= Security.getProviders();
//		
//		for(Provider e : bb)
//		{
//			System.out.println(e.getName());
//			for(Service b : e.getServices())
//			{
//				System.out.println("-- " + b.getAlgorithm());
//			}
//			
//		}
		
		
	    generator.initialize(1024, random);
	    KeyPair pair = generator.generateKeyPair();
	    pubKey = pair.getPublic();
	    privKey = pair.getPrivate();
	    
	    
	    FileOutputStream fstr=null;
	    ObjectOutputStream oos = null;
	    
		try {
			fstr = new FileOutputStream(System.getProperty("user.home")+"/keys/pubkey.key");
			oos= new ObjectOutputStream(fstr);
			oos.writeObject(pubKey);
			fstr.close();
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public byte[] Encrypt(String data)
	{
		SecureRandom random = new SecureRandom();
		byte[] cipherText=null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, pubKey,random);
			cipherText = cipher.doFinal(data.getBytes());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cipherText;
	}
	
	public byte[] Decrypt(byte[] data)
	{
		SecureRandom random = new SecureRandom();
		byte[] plainText=null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, privKey,random);
			plainText = cipher.doFinal(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return plainText;
	}
	
	public Key getPubKey()
	{
		return pubKey;
	}
	
	public void setPubKey(Key key)
	{
		this.pubKey=key;
	}
	
	
	public Key getPrivKey()
	{
		return privKey;
	}
	
	public void setPrivKey(Key key)
	{
		this.privKey=key;
	}
}
