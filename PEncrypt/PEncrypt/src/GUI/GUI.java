package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import Encryption.EncryptionManager;

public class GUI {

	private JFrame frame;

	private EncryptionManager man;

	private String pathToPasswords;

	private JMenuItem save;

	private JTable table;

	public GUI(EncryptionManager man) {

		pathToPasswords = "";

		this.man = man;
		prepareGUI();
		askFile();

		frame.setVisible(true);
	}

	public void openEncryptedFile() throws IOException {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Open encrypted data");
		fc.setAcceptAllFileFilterUsed(false);
		FileFilter filter = new FileNameExtensionFilter("PEncrypt Data file","PEdata");
		fc.addChoosableFileFilter(filter);
		
		int returnVal = fc.showOpenDialog(frame);
		

		if (returnVal == JFileChooser.APPROVE_OPTION)
		{

			File file = fc.getSelectedFile();
			pathToPasswords = file.getAbsolutePath();
			int size = 0;

			FileInputStream fin = null;
			ObjectInputStream input = null;

			try {
				fin = new FileInputStream(fc.getSelectedFile());
				input = new ObjectInputStream(fin);

				size = (int) input.readObject();
			} catch (Exception e) {

				e.printStackTrace();
			}

			for (int i = 0; i < size; i++) {
				byte[] line = null;
				try {
					line = (byte[]) input.readObject();
				} catch (ClassNotFoundException e) {

					e.printStackTrace();
				}

				String data = new String(man.Decrypt(line));

				String[] values = data.split("-");
				insertRow(values);

			}
			fin.close();
			input.close();
			
		}
		
		if(returnVal == JFileChooser.CANCEL_OPTION)
		{
			System.exit(1);
		}
	}

	private void insertRow(String[] values) 
	{
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.addRow(values);

	}

	private void askFile() {
		String[] options = { "Open", "Create" };

		int returnval = JOptionPane.showOptionDialog(frame, "Do you want create or open a password file?", "PEncrypt",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

		switch (returnval) 
		{
		case JOptionPane.YES_OPTION:
			openKey();
			save.setEnabled(true);
			try {
				openEncryptedFile();
			} catch (IOException e) {

				e.printStackTrace();
			}
			System.out.println("yes");

			break;

		case JOptionPane.NO_OPTION:
			System.out.println("no");
			man.genKeyPair();
			saveKey();
			break;
		case JOptionPane.CLOSED_OPTION:
			System.exit(1);
			break;
		}

	}

	private void saveKey() {

		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Save decryption key");
		fc.setAcceptAllFileFilterUsed(false);
		FileFilter filter = new FileNameExtensionFilter("PEncrypt Decryption Key","key");
		fc.addChoosableFileFilter(filter);
		
		int returnVal = fc.showSaveDialog(frame);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = new File(fc.getSelectedFile().getPath() + ".key");
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			FileOutputStream fstr = null;
			ObjectOutputStream oos = null;

			try {
				fstr = new FileOutputStream(file);
				oos = new ObjectOutputStream(fstr);
				oos.writeObject(man.getPrivKey());

				oos.close();
				fstr.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		if(returnVal == JFileChooser.CANCEL_OPTION)
		{
			System.exit(1);
		}
	}

	private void openKey() {
		// TODO Auto-generated method stub
		JFileChooser fc = new JFileChooser();
		fc.setAcceptAllFileFilterUsed(false);
		fc.setDialogTitle("Open decryption key");
		FileFilter filter = new FileNameExtensionFilter("PEncrypt Decryption Key","key");
		fc.addChoosableFileFilter(filter);
		int returnVal = fc.showOpenDialog(frame);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			FileInputStream fin = null;
			ObjectInputStream input = null;
			Key key = null;
			
			try {
				fin = new FileInputStream(fc.getSelectedFile());
				input = new ObjectInputStream(fin);
				key = (Key) input.readObject();
				fin.close();
				input.close();
			} catch (Exception e) {

				e.printStackTrace();
			}
			
			man.setPrivKey(key);
			
		}
		if(returnVal == JFileChooser.CANCEL_OPTION)
		{
			System.exit(1);
		}

		openPubKey();
	}

	private void openPubKey() {

		FileInputStream fin = null;
		ObjectInputStream input = null;
		Key key = null;
		try {
			fin = new FileInputStream(new File(System.getProperty("user.home") + "/keys/pubkey.key"));
			input = new ObjectInputStream(fin);
			key = (Key) input.readObject();
			fin.close();
			input.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

		man.setPubKey(key);

	}

	public void prepareGUI() {
		frame = new JFrame("PEncrypt");
		frame.setSize(400, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.LINE_AXIS));

		JMenuBar menuBar = new JMenuBar();

		JMenu menu = new JMenu("File");

		save = new JMenuItem("Save");
		save.setEnabled(false);
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("nuthin");
				
				File file = new File(pathToPasswords);
				saveToFile(file);
				
			}
		});
		
		JMenuItem saveAs = new JMenuItem("Save as");
		saveAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("I swear o.o");
				save.setEnabled(true);
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Save encrypted data");
				fc.setAcceptAllFileFilterUsed(false);
				FileFilter filter = new FileNameExtensionFilter("PEncrypt Data file","PEdata");
				fc.addChoosableFileFilter(filter);
				
				int returnVal = fc.showSaveDialog(frame);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = new File(fc.getSelectedFile().getPath() + ".PEdata");
					pathToPasswords = file.getAbsolutePath();
					saveToFile(file);
				}
			}
		});

		menu.add(save);
		menu.add(saveAs);

		menuBar.add(menu);
		String[] columnNames = { "Username", "Password", "Description", };

		table = new JTable(new DefaultTableModel(new Object[] { "Username", "Password", "Description" }, 0));

	

		JPanel buttons = new JPanel();

		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));

		JButton addR = new JButton("Add Row");
		addR.setAlignmentX(buttons.CENTER_ALIGNMENT);
		addR.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.addRow(new Object[] { "", "", "" });

			}

		});

		JButton removeR = new JButton("Remove Row");
		removeR.setAlignmentX(buttons.CENTER_ALIGNMENT);
		removeR.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.removeRow(model.getRowCount() - 1);

			}

		});

		frame.getContentPane().add(new JScrollPane(table));
		buttons.add(addR);
		buttons.add(removeR);
		frame.getContentPane().add(buttons);

		frame.setJMenuBar(menuBar);

	}
	
	private void saveToFile(File file)
	{
		FileOutputStream fstr = null;
		ObjectOutputStream oos = null;

		try {
			fstr = new FileOutputStream(file);
			oos = new ObjectOutputStream(fstr);
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		ArrayList<String> temp = new ArrayList<>();
		for (int i = 0; i < table.getRowCount(); i++) {
			String line = "";
			for (int j = 0; j < table.getColumnCount(); j++) 
			{
				line = line + table.getValueAt(i, j) + "-";
			}
			temp.add(line);
		}
		
		try {
			oos.writeObject(temp.size());
			for (String str : temp) {
				oos.writeObject(man.Encrypt(str));
			}
				fstr.close();
				oos.close();
		} catch (Exception e2) {

			e2.printStackTrace();
		}	
	}

}
